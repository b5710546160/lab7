/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public interface Unit {
	/**
	 * 
	 * @param a Type of Value we want 
	 * @param amt Value
	 * @return new Value
	 */
	public double convertTo(Unit a,double amt);
	/**
	 * 
	 * @return Value is we want.
	 */
	public double getValue();
	/**
	 * 
	 * @return Name of Type
	 */
	public String toString();
}
