/**
 * 
 * @author Kitipoom Kongpetch
 *
 */

public enum Length implements Unit {
	METER("meter", 1.0), CENTRIMETER("centrimeter", 0.01), KILOMETER(
			"kilometer", 1000.0), MILE("mile", 1609.344), FOOT("foot", 0.30480), WA(
			"wa", 2.0);
	public final double value;
	public final String name;

	private Length(String name, double value) {
		this.value = value;
		this.name = name;
	}
	/**
	 * 
	 * @param a name of type
	 * @return Unit
	 */
	public Length check(String a) {
		if (a.equals("meter")) {
			return Length.METER;
		}
		if (a.equals("centrimeter")) {
			return Length.CENTRIMETER;
		}
		if (a.equals("kilometer")) {
			return Length.KILOMETER;
		}
		if (a.equals("mile")) {
			return Length.MILE;
		}
		if (a.equals("foot")) {
			return Length.FOOT;
		}
		if (a.equals("wa")) {
			return Length.WA;
		}
		return null;
	}

	@Override
	public double convertTo(Unit u, double amt) {

		Length base = check(this.name);
		Length sub = check(u.toString());
		double result = amt * base.getValue() / sub.getValue();
		return result;
	}

	@Override
	public double getValue() {

		return this.value;
	}

	@Override
	public String toString() {
		return this.name;

	}

	/*
	 * public static void main(String [] args) { Length mile=Length.KILOMETER;
	 * Length km=Length.METER; System.out.print(mile.convertTo(km, 3.0));
	 * 
	 * }
	 */
}
