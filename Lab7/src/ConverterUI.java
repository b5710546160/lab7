import javax.swing.*;
import javax.swing.event.AncestorListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class ConverterUI extends JFrame {
	// attributes for graphical components
	private UnitConverter uc = new UnitConverter();
	private JButton convertButton, clearButton;
	private UnitConverter unitconverter;
	private JTextField inputField2;
	private JTextField inputField1;
	private JComboBox combobox1;
	private JComboBox combobox2;
	
	private static Unit[] length;

	// The class needs a reference to the
	// unitconverter and objects it calls.
	/**
	 * 
	 * @param uc
	 *            UnitConverter
	 */
	public ConverterUI(UnitConverter uc) {
		this.unitconverter = uc;
		this.length = uc.getUnits();
		this.setTitle("Simple Converter");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponents();
		this.pack(); // resize the Frame to match size of components
	}

	/**
	 * initialize components in the window.
	 */
	private void initComponents() {

		// manager. Create event listeners and add them to components.

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout();
		contents.setLayout(layout);
		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");
		inputField2 = new JTextField(10);
		inputField2.setEnabled(false);
		inputField1 = new JTextField(10);
		combobox1 = new JComboBox(uc.getUnits());
		combobox2 = new JComboBox(uc.getUnits());
		//
		contents.add(inputField1);
		contents.add(combobox1);
		contents.add(inputField2);
		contents.add(combobox2);
		contents.add(convertButton);
		contents.add(clearButton);
		ActionListener listener = new ConvertButtonListener();
		convertButton.addActionListener(listener);
		ActionListener listener1 = new ClearButtonListener();
		clearButton.addActionListener(listener1);
		inputField1.addActionListener(listener);
	}
	/**
	 * 
	 * @param str String
	 * @return String is number or not
	 */
	public  boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			//inputField2.setText("false " );
			return false;
		}
		return true;
	}

	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed. */
		public void actionPerformed(ActionEvent evt) {
			String s = inputField1.getText().trim();
			Unit unit1 = (Unit) combobox1.getSelectedItem();
			Unit unit2 = (Unit) combobox2.getSelectedItem();
			// This line is for testing. Comment it out after you see how it
			// works.

			System.out.println("actionPerformed: input=" + s);
			if (s.length() > 0) { // convert kilometer to mile
				double value = 0;
				if (isNumeric(s)) {
					inputField1.setForeground (Color.black);
					value = Double.valueOf(s);
					UnitConverter converter = new UnitConverter();
					double result = converter.convert(value, unit1, unit2);
					inputField2.setText(" " + result);
				} else {
					JOptionPane.showMessageDialog(ConverterUI.this,"Invalid");
					inputField1.setForeground (Color.red);
					// throw new NumberFormatException();
				}

			}

		}
	} // end of the inner class

	class ClearButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			inputField2.setText(" ");
			inputField1.setText(" ");

		}

	}
	/**
	 * 
	 * @param args runprogram
	 */
	public static void main(String[] args) {
		UnitConverter uc = new UnitConverter();
		JFrame a = new ConverterUI(uc);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setVisible(true);
	}
}
