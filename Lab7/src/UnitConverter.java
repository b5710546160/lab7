/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class UnitConverter {
	/**
	 * 
	 */
	public  UnitConverter (){
		
	}
	/**
	 * 
	 * @param amount value we want to convert
	 * @param fromUnit Unitbase
	 * @param toUnit target Unit
	 * @return value is convert
	 */
	public double convert(double amount,Unit fromUnit, Unit toUnit){
		return fromUnit.convertTo(toUnit, amount);
		
	}
	/**
	 * 
	 * @return All type unit
	 */
	public Unit[] getUnits(){
		Unit[] a= Length.values();
		return a;
		
	}
	
	/**
	 * 
	 * @param args
	 */
	 /*public static void main(String [] args) {
		UnitConverter uc =  new UnitConverter();
		uc.getUnits( );
		
		System.out.print(uc.convert( 3.0, Length.KILOMETER, Length.METER ));
		
	}*/
}
